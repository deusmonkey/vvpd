#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Как я не хочу комментить это(. Но...
Это также основной модуль, как main.py, в нем содержатся все визуальные виджеты,
которые соединены, весь функционал аквы"""
import operator
import webbrowser
import platform
import re
import webbrowser
import pickle
import easygui
import os
import platform
import requests
import json
import subprocess
import random
import datetime

import configparser
import threading
from PyQt5.QtWidgets import QWidget, QMessageBox, QInputDialog, QMessageBox, QMainWindow, QLabel, \
    QDialog, QApplication, QGridLayout, QSlider, QPushButton, QStackedLayout, QFrame
from PyQt5.QtCore import pyqtSignal, QThread, pyqtSlot, QObject, Qt, QTimer, QSize, QRect
from PyQt5.QtGui import QPixmap, QIcon, QMovie
from collections import OrderedDict

import simpleaudio as sa

from dialog_creat import Ui_Form
from enter_wind import EnterForm
from Weather_widget import WeatherForm
import wordskey
import Linux_logic
import Character_setup


# from settings import Settings


class CustomWindow(QMainWindow):
    """
    Так-ссс, этот класс основное окно, которое связывает все, принимает только размеры.
    """

    def __init__(self, config, flow, parent=None):
        super(CustomWindow, self).__init__(parent)
        self.config = config
        character = config["User"]["character"]
        self.flow = flow
        # Натройка визуала самого окна, прозрачноть и т д
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setWindowIcon(QIcon("ImageGif/Gif/%s_set/Icon.png" % (character)))

        self.path = config["Path"]["%s" % (character)]
        self.size_win = int(config["Settings"]["window_scale"])
        # Размеры окна

        self.config = config

        self.child = None
        self.initUI()

        # Флаг для того, чтобы аква весела поверх всех окон)))
        self.setWindowFlag(Qt.WindowStaysOnTopHint)

    @pyqtSlot(int)
    def size_gif_change(self, size):
        self.l.m.stop()
        self.l.size_win = size
        self.l.setMovie(self.l.m)
        self.l.m.start()
        self.l.adjustSize()

    @pyqtSlot(int)
    def size_change(self, size):
        self.win_size = [(int(self.config["Settings"]["Window_size_w"]) *
                          size / 100) * 1.1,
                         (int(self.config["Settings"]["Window_size_h"]) *
                          size / 100) * 1.1]
        self.setGeometry(50, 50, self.win_size[0], self.win_size[1])
        screen_geometry = QApplication.desktop().availableGeometry()
        screen_size = (screen_geometry.width(), screen_geometry.height())
        win_size = (self.frameSize().width(), self.frameSize().height())
        x = screen_size[0] - win_size[0]
        y = screen_size[1] - win_size[1]
        self.move(x, y)

    # Тут бред))), который из всех окон делает виджет и размещает его на акве
    def initUI(self):
        self.size_change(self.size_win)

        self.child = Setup_size_window(config=self.config, win_CW=self)
        self.child.size_CW.connect(self.size_change)
        self.child.size_gif.connect(self.size_gif_change)

        self.gif_display()

        self.enterWindow = EnterWindow(win_size=self.child)  # Виджет ввода
        self.flow.change_text_enter.connect(self.enterWindow.say.setText)
        self.weatherWindow = WeatherForm(self.child)  # Виджет погоды, для функции информации о погоде
        self.quoteWindow = DialogWindow(self, win_size=self.child)  # Виджет диологового окна аквы - облачко
        self.quoteWindow.start_manual.connect(self.manual)

        self.flow.start_new_gif.connect(self.change_gif)
        self.flow.change_text.connect(self.quoteWindow.textEdit.setText)

        self.tab1 = QWidget()
        quoteLayout = QStackedLayout()
        quoteLayout.setStackingMode(1)
        mframe = QFrame(self.tab1)
        hframe = QFrame(self.tab1)
        wframe = QFrame(self.tab1)
        lay = QGridLayout(mframe)
        lay.setRowStretch(0, 1)
        lay.setRowStretch(1, 1)
        # lay.setRowStretch(2, 1)
        lay.setColumnStretch(0, 1)
        lay.setColumnStretch(1, 1)
        # lay.setColumnStretch(2, 1)
        lay.addWidget(self.quoteWindow, 0, 0)
        mframe.setLayout(lay)

        lay2 = QGridLayout(hframe)
        lay2.setRowStretch(0, 5)
        lay2.setRowStretch(1, 1)
        lay2.setColumnStretch(0, 5)
        lay2.setColumnStretch(1, 1)
        lay2.addWidget(self.enterWindow, 1, 0)

        lay3 = QGridLayout(wframe)
        lay3.setRowStretch(0, 1)
        lay3.setRowStretch(1, 1)
        lay3.setRowStretch(2, 1)
        lay3.setColumnStretch(0, 1)
        lay3.setColumnStretch(1, 1)
        lay3.setColumnStretch(2, 1)
        lay3.addWidget(self.weatherWindow, 0, 2)

        quoteLayout.addWidget(hframe)
        quoteLayout.addWidget(mframe)  #
        quoteLayout.addWidget(wframe)
        self.tab1.setLayout(quoteLayout)
        # self.setCentralWidget(self.tab1)
        self.setCentralWidget(self.tab1)
        self.enterWindow.hide()
        self.quoteWindow.hide()

        # self.settings_win = Settings(win_CW=self)
        self.choi_size_settin = 0

        # Меняем гифки по времени
        self.timer_gif = QTimer(self)
        self.timer_gif.timeout.connect(self.changes_gif_time)
        self.flow.timer_gif.connect(self.timer_gif.start)

        self.timer_weather = QTimer(self)
        self.timer_weather.timeout.connect(self.changes_weat_time)

        self.timer_exit = QTimer(self)
        self.timer_exit.timeout.connect(self.changes_exit_time)
        self.flow.timer_exit.connect(self.timer_exit.start)

    def changes_gif_time(self):
        self.flow.start_new_gif.emit(self.flow.base_gif)
        self.timer_gif.stop()

    def changes_weat_time(self):
        self.weatherWindow.hide()
        self.timer_weather.stop()

    def changes_exit_time(self):
        self.close()
        self.timer_exit.stop()

    @pyqtSlot(str)
    def change_gif(self, path):
        self.l.m.stop()
        self.l.m.setFileName(path)
        self.l.m.start()

    # Изменение размеров
    def show_size_setup(self):
        if self.choi_size_settin == 0:
            self.choi_size_settin = 1
            self.child.show()

    def change_character(self):
        self.CHS = Character_setup.Character_setup(self.config)
        self.CHS.show()

    # Аква
    def gif_display(self):
        self.l = QMovieLabel(self.path, self.config, self)
        self.l.adjustSize()
        self.l.show()

    def exit(self):
        self.flow.timer_exit.emit(3000)
        self.show()
        self.flow.start_new_gif.emit(self.flow.speak_gif)
        self.flow.change_text.emit("- Прощай...")
        self.quoteWindow.show()
        self.flow.listener.stop()

    def change_weather(self, weather):
        self.weatherWindow.show()
        self.timer_weather.start(3000)
        if weather == "ясно":
            self.weatherWindow.change_icon(weather)

        if weather == "пасмурно" or weather == "гроза с небольшим дождём":
            self.weatherWindow.change_icon(weather)
            # self.weatherWindow.show()

        if weather == "облачно с прояснениями":
            self.weatherWindow.change_icon(weather)
            # self.weatherWindow.show()

        if weather == "переменная облачность":
            self.weatherWindow.change_icon(weather)
            # self.weatherWindow.show()

        if weather == "небольшая облачность":
            self.weatherWindow.change_icon(weather)
            # self.weatherWindow.show()

        if weather == "небольшой дождь":
            self.weatherWindow.change_icon(weather)

        if weather == "небольшой проливной дождь":
            self.weatherWindow.change_icon(weather)

    @pyqtSlot()
    def manual(self):
        QMessageBox.about(self, 'О программе:',
                          'О программе:',
                          'Anime interactive computer assistant - Aica (Айка)\n'
                          'Привет, Я Айка - десктопный помощник!!! К сожалению, своего облика у меня нет, '
                          'но я могу принимать облики других персонажей.\n'
                          'Надевая эти облики, я с радостью помогаю моему пользователю и скрашиваю его времяпровождение за компьютером.\n'
                          'Ведь я могу открывать различные сайты, папки, и вообще все что можно открыть.\n'
                          'Могу развлечь тебя небольшими играми и рассказать последние новости.\n'
                          'Ну или ты можешь просто любоваться мной на своем рабочем столе.\n'
                          'Подробнее:\n'
                          '1. Могу открыть что-то (папку, файлы, сайты, браузер)\n'
                          '2. Поговорить с вами\n'
                          '3. Сказать информацию о вашей системе, времени, погоде'
                          '4. Вы также можете добавить в быстрый запуск папки и файлы для открытия\n'
                          '5. А еще можете изменить браузер, который я открываю\n'
                          '6. Сыграть в орла и решку\n'
                          '7. Могу сделать предсказание, но это не точно\n'
                          '8. Сказать где вы\n'
                          '9. Рассказать хокку\n'
                          'И многое другое... Все зависит от разрабов("\n'
                          'Меню:\n'
                          '1. Save - начать с изменениями (если они есть)\n'
                          '2. Cancel - начать и отменить измения (если они есть)\n'
                          '3. Exit - вызод')


# Отрисовка Гифки, принимает путь до гифки, и размеры её
class QMovieLabel(QLabel):
    def __init__(self, filename, config, parent=None):
        super(QMovieLabel, self).__init__(parent)
        self.m = QMovie(filename)
        self.config = config
        self.size_win = int(self.config["Settings"]["window_scale"])
        self.setMovie(self.m)
        self.m.start()

        # Расположение аквы на окне

    # Размеры аквы
    def setMovie(self, movie):
        super(QMovieLabel, self).setMovie(movie)

        s = movie.currentImage().size()
        self._movieWidth = s.width()
        self._movieHeight = s.height()
        movie = self.movie()

        x = (int(self.config["Settings"]["Window_size_w"]) *
             self.size_win / 100)
        y = (int(self.config["Settings"]["Window_size_h"]) *
             self.size_win / 100)
        movie.setScaledSize(QSize(x, y))
        self.adjustSize()
        self.move(90 *
                  self.size_win / 100, 55 *
                  self.size_win / 100)


class Setup_size_window(QDialog):
    """
    Так-ссс, это класс - окно для изменения размеров аквы.
    """
    size_gif = pyqtSignal(int)
    size_ent = pyqtSignal(int)
    size_CW = pyqtSignal(int)
    size_dialog = pyqtSignal(int)
    size_weather = pyqtSignal(int)

    def __init__(self, config, win_CW):
        super(Setup_size_window, self).__init__()
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowFlag(Qt.WindowStaysOnTopHint)
        self.setWindowIcon(QIcon("ImageGif/Image/settings.png"))
        self.win_CW = win_CW
        self.size = []
        self.value = int(config["Settings"]["window_scale"])
        self.config = config
        self.initUI()

    def initUI(self):
        self.setGeometry(50, 50, 200, 150)

        screen_geometry = QApplication.desktop().availableGeometry()
        screen_size = (screen_geometry.width(), screen_geometry.height())
        x = screen_size[0] - int(self.config["Settings"]["Window_size_w"])
        y = screen_size[1] - int(self.config["Settings"]["Window_size_h"])
        self.move(x, y)

        Glayout = QGridLayout(self)

        # ==========================
        self.label_1 = QLabel(self)
        self.label_1.setGeometry(QRect(0, 0, 200, 150))
        self.pixmap = QPixmap("ImageGif/Image/Tray1.png")
        self.mini_pix = self.pixmap.scaled(200, 150, Qt.IgnoreAspectRatio, Qt.FastTransformation)
        self.label_1.setPixmap(self.mini_pix)
        self.line1 = QLabel(self)
        self.line1.setText(str(self.value) + "%")
        self.line1.setStyleSheet('''
                                           font:  16px;
                                           color: white;
                                           font-style: italic;
                                                   ''')
        Glayout.addWidget(self.line1, 1, 2)

        self.slider = QSlider(Qt.Horizontal)
        self.slider.setFocusPolicy(Qt.StrongFocus)
        self.slider.setTickPosition(QSlider.TicksBothSides)
        self.slider.setTickInterval(25)
        self.slider.setSingleStep(5)
        self.slider.setRange(25, 200)
        self.slider.setValue(self.value)
        self.slider.valueChanged[int].connect(self.change_value)
        Glayout.addWidget(self.slider, 1, 1)

        self.bt1 = QPushButton("Ok", self)
        self.bt1.clicked.connect(self.on_click)
        self.bt1.setStyleSheet(''' QPushButton:hover {background-color: white;
                                                            color: #002756;}
                                QPushButton:!hover {background-color: rgba(0, 0, 0, 00);
                                                                    border-width: 2px;
                                                                    border-style: solid;
                                                                    font:  16px;
                                                                    color: white;
                                                                    font-style: italic;
                                                                    padding: 1px;}
                                                                     ''')
        Glayout.addWidget(self.bt1, 3, 2)

        self.cbt = QPushButton("Cancel", self)
        self.cbt.clicked.connect(self.cancel)
        self.cbt.setStyleSheet(''' QPushButton:hover {background-color: white;
                                                            color: #002756;}
                                QPushButton:!hover {background-color: rgba(0, 0, 0, 00);
                                                                    border-width: 2px;
                                                                    border-style: solid;
                                                                    font:  16px;
                                                                    color: white;
                                                                    font-style: italic;
                                                                    padding: 1px;}
                                                                     ''')
        Glayout.addWidget(self.cbt, 3, 1)

        # ==========================

        # ==========================

        self.setLayout(Glayout)

    def cancel(self):
        self.win_CW.choi_size_settin = 0
        self.close()

    def change_value(self, value):
        self.value = value
        self.line1.setText(str(self.value) + "%")

    def on_click(self):
        width = int(self.config["Settings"]["Window_size_w"])
        height = int(self.config["Settings"]["Window_size_h"])
        width = width * self.value / 100
        height = height * self.value / 100
        self.size_CW.emit(int(self.value))
        self.size_gif.emit(int(self.value))
        self.size_dialog.emit(int(self.value))
        self.size_ent.emit(int(self.value))
        self.size_weather.emit(int(self.value))
        self.config["Settings"]["window_scale"] = str(self.value)

        with open("Data/Config.ini", 'w') as configfile:
            self.config.write(configfile)
        self.win_CW.choi_size_settin = 0
        self.close()


class EnterWindow(EnterForm):
    """
    Класс - наследник, от окна ввода
    """

    def __init__(self, win_size):
        super().__init__()
        self.win_size = win_size
        self.win_size.size_ent.connect(self.size_change)
        self.win2 = 0  # Чтобы связать, окно ввода и окно вывода
        self.edit_line.returnPressed.connect(self.input_comm)

    @pyqtSlot(int)
    def size_change(self, size):
        width = 445 * size / 100
        height = 100 * size / 100
        # print("check ", size)
        self.scale = int(size / 100 * 18)
        self.back.setStyleSheet('''
                                background-color: %s;
                                border-width: 2px;
                                border-style: solid;
                                border-radius: 10px;
                                border-color: %s;
                                min-width: 10em;
                                min-height: 5em;
                                font: %s;
                                ''' % (self.bg_color,
                                       self.border_color,
                                       str(self.scale) + 'px'))
        self.edit_line.setStyleSheet('''
                                     background-color: rgba(0, 0, 0, 00);
                                     border-width: 2px;
                                     border-style: solid;
                                     border-radius: 10px;
                                     border-color: %s;
                                     font:  %s;
                                     color: %s;
                                     font-style: italic;
                                     min-width: 10em;
                                     padding: 6px;
                                     ''' % (self.border_color,
                                            self.font_color,
                                            str(self.scale) + 'px'))
        self.label.setStyleSheet('''
                                    font:  %s;
                                    color: %s;
                                    font-style: italic;
                                    padding: 6px;
                                    ''' % (str(self.scale) + 'px',
                                           self.font_color))
        self.say = QLabel(self.hframe)
        self.say.setText("Sample text")
        self.say.setObjectName("UserSayed")
        self.say.setStyleSheet('''
                                    font:  %s;
                                    color: %s;
                                    font-style: italic;
                                    padding: 6px;
                                    ''' % (str(self.scale) + 'px',
                                           self.font_color))
        # self.back.setGeometry(QRect(0, 0, 445 * size / 100,
        #                                  100 * size / 100))
        # self.mini_pix = self.pixmap.scaled(width, height, Qt.IgnoreAspectRatio, Qt.FastTransformation)

    # self.back.setPixmap(self.mini_pix)

    # Функция для обработки разных команд
    def input_comm(self):
        comm = f'{self.edit_line.text()} '

        if len(comm) > 1:
            self.win2.input_comm(f'{comm}')
            self.edit_line.clear()
            self.say.setText(comm)
        else:
            self.win2.textEdit.setText("Вы ничего не ввели.")


def thread(my_func):
    """
    Запускает функцию в отдельном потоке
    """

    def wrapper(*args, **kwargs):
        my_thread = threading.Thread(target=my_func, args=args, kwargs=kwargs)
        my_thread.start()

    return wrapper


@thread
def city_and_weather(signal, choi):
    try:
        send_url = "http://api.ipstack.com/check?access_key=c024957c288f813bf6f290a7182aa3d7"
        geo_req = requests.get(send_url)
        geo_json = json.loads(geo_req.text)
        if choi == 0:
            signal.emit(geo_json)
            return 0

        elif choi == 1:
            city_id = geo_json["location"]["geoname_id"]
            appid = "3afeacd4d791d087699e1eef4315c1ec"
            try:
                res = requests.get("http://api.openweathermap.org/data/2.5/weather",
                                   params={'id': city_id, 'units': 'metric', 'lang': 'ru', \
                                           'APPID': appid})
                data = res.json()
                signal.emit(data)
                return 0

            except:
                signal.emit({0: "К сожалению сервер не отвечает.\n"
                                "Попробуйте позже."})
                return 0
    except:
        signal.emit({0: "Похоже нет доступа к сети.\n"
                        "Попробуйте позже."})
        return 0


class DialogWindow(Ui_Form):
    """
    Так-ссс... Облачко (хз почему облачко), которое содержит весь функционал и почти все фразы аквы.
    """

    start_manual = pyqtSignal()
    th_signal_loc = pyqtSignal(dict)
    th_signal_weath = pyqtSignal(dict)

    def __init__(self, CW, win_size):
        super().__init__()
        self.CW = CW  # Для функции погоды
        self.win_size = win_size
        self.th_signal_loc.connect(self.show_location, Qt.QueuedConnection)
        self.th_signal_weath.connect(self.show_weather, Qt.QueuedConnection)
        self.win_size.size_dialog.connect(self.size_change)
        self.num_brow = None  # Для изменения основного бразузера
        self.CW.flow.proc_comm.connect(self.input_comm)
        self.OPERATIONS = {
            '+': operator.add,
            '-': operator.sub,
            '*': operator.mul,
            '/': operator.floordiv,
            '^': operator.pow,
        }

    @pyqtSlot(int)
    def size_change(self, size):
        self.label.setGeometry(QRect(0, 0, 260 * size / 100,
                                     300 * size / 1.00))

        self.mini_pix = self.pixmap.scaled(260 * size / 100,
                                           300 * size / 100,
                                           Qt.KeepAspectRatio, Qt.FastTransformation)

        self.label.setPixmap(self.mini_pix)

    # Обработка команд
    @pyqtSlot(str)
    def input_comm(self, command):
        # self.textEdit.append(f'Вы сказали - {command}')
        command = wordskey.words_recog(command)
        self.do_command(command)
        # Немного звуков. Не пугайтесь
        filename = "Sounds/Kaguya/O_kawai.wav"
        wave_obj = sa.WaveObject.from_wave_file(filename)
        play_obj = wave_obj.play()

    def get_number(self, varstr):
        s = ""
        if varstr[0] == '-':
            s += "-"
            varstr = varstr[1:]
        for c in varstr:
            if not c.isdigit():
                break
            s += c
        return (int(s), len(s))

    def perform_operation(self, string, num1, num2):
        op = self.OPERATIONS.get(string, None)
        if op is not None:
            return op(num1, num2)
        else:
            return None

    def eval_math_expr(self, expr):
        while True:
            try:
                number1, end_number1 = self.get_number(expr)
                expr = expr[end_number1:]
                if expr == '':
                    return number1
                op = expr[0]
                expr = expr[1:]
                number2, end_number2 = self.get_number(expr)
                number1 = self.perform_operation(op, number1, number2)
            except Exception as e:
                break

            return number1

    def open_file_dir(self, result):
        with open("Pickle/path_base_win.pickle", "rb") as f:
            path = pickle.load(f)
        for i in path:
            i = r"\b" + f"{i}" + r"\b"
            i = re.search(i, result)
            if i:
                os.startfile(path[i[0]])
                return 0
        self.textEdit.setText("- Такой папки/файла нет.")

    def browser(self, ans, brow, choi):
        if choi == 0:
            self.textEdit.setText("- Открываю браузер (сайт - 'Яндекс').")
            webbrowser.get(brow).open("https://yandex.ru")
            return 0

        elif choi == 1:

            with open("Pickle/site_base.pickle", "rb") as f:
                data_site = pickle.load(f)

            for key in data_site:
                key = r"\b" + f"{key}" + r"\b"
                key = re.search(key, ans)
                if key:
                    self.textEdit.setText(f"- Открываю {key[0]} в браузере.")
                    webbrowser.get(brow).open(key[0])
                    return 0

            self.textEdit.setText("- Я не знаю такого сайта")


        elif choi == 3:
            self.textEdit.setText("- Открываю новости.")
            webbrowser.get(brow).open("http://newslab.ru/news/")

        elif choi == 4:
            self.textEdit.setText("- Открываю новости про короновирус.")
            webbrowser.get(brow).open("https://coronavirus-monitor.ru/")

        elif choi == 2:
            check_browser = ['Google', 'Netscape', 'Mozilla', 'Opera',
                             'Konqueror', 'Yandex', 'Safari', 'lynx']
            self.textEdit.setText("Выберите другой БРАУЗЕР в проводнике.")
            choice_br = easygui.fileopenbox(filetypes=['*.exe'], default="*.exe")
            ok = -1
            if choice_br != None:
                if str(choice_br[-4:]) == '.exe':
                    for i in range(len(check_browser)):
                        ok = choice_br.find(check_browser[i])
                        if ok != -1:
                            break
                    if ok == -1:
                        text, ok = QInputDialog.getText(self, 'Ввод ответа',
                                                        'Я не знаю такого браузера. '
                                                        'Вы уверены, что хотите изменить браузер?\n'
                                                        'Напишете да, иначе этот браузер не будет выбран.')
                        if ok and text:
                            text = str(text).lower()
                            if text == 'да':
                                return choice_br

                        else:
                            QMessageBox.warning(self, "Ошибка", "Вы ничего не ввели (отмена операции)")
                    elif ok != -1:
                        return choice_br
                else:
                    self.textEdit.setText('- Вы не выбрали браузер.')
                    return 0
            else:
                self.textEdit.setText('- Вы не выбрали браузер.')
                return 0

    def mon(self):
        chose = ["- Орёл", "- Решка"]
        self.textEdit.setText(random.choice(chose))

    def boll(self):
        chose = ["- Нет", "- Да", "- Возможно", "- Не знаю", "- Скорее нет", \
                 "- Какая разница, ВРЕМЯ СМОТРЕТЬ АНИМЕЕЕЕЕЕ"]
        self.textEdit.setText(random.choice(chose))

    def time_now(self, dat_time):
        now = datetime.datetime.now()
        answer = ["Время", "Дата"]
        if dat_time == answer[0]:
            return f"- Сейчас - {now.hour}:{now.minute}:{now.second}. Пора смотреть аниме (^-^)!!!"
        elif dat_time == answer[1]:
            return f"- Сегодняшняя дата - {now.date().day}-{now.date().month}-{now.date().year}. " \
                   f"Не забудьте отдохнуть (^-^)."

    def system(self):
        sys = platform.uname()
        return "Информация о системе:\n" \
               f"    Имя системы/OS - {sys[0]}\n" \
               f"    Сетевое имя компьютера - {sys[1]}\n" \
               f"    Выпуск системы - {sys[2]}\n" \
               f"    Версия выпуска системы - {sys[3]}\n" \
               f"    Тип машины - {sys[4]}\n" \
               f"    Имя процессора - {sys[5]}"

    def show_weather(self, weath_data):
        if 'name' in weath_data:
            self.textEdit.setText("- Погода на сегодня:")
            self.textEdit.append(f"{weath_data['weather'][0]['description']},")
            self.textEdit.append(f"темп-ра: {weath_data['main']['temp']} гр.,")
            self.textEdit.append(f"(min: {weath_data['main']['temp_min']}, "
                                 f"max: {weath_data['main']['temp_max']})")
            weat = weath_data['weather'][0]['description']
            self.CW.change_weather(weat)
        else:
            self.textEdit.setText(f"{weath_data[0]}")

    def show_location(self, locat_data):
        if "city" in locat_data:
            locat_data = locat_data['city']
            self.textEdit.setText(f"- Ваше примерное местоположение - {locat_data}")
        else:
            self.textEdit.setText(f"{locat_data[0]}")

    def do_command(self, result):
        self.show()
        self.textEdit.setText("- Выполняю...")
        lin_log = Linux_logic.MainLogicLinux()

        if platform.system() == "Linux":
            # print(result)
            if re.search(r"\bбраузер\b", f"{result}"):
                lin_log.open_browser()
            elif re.search(r"\bоткрой\b", f"{result}") and re.search(r"\bсайт\b", f"{result}"):
                result_site = wordskey.site_recog(result)
                lin_log.open_site(url=result_site)
            elif re.search(r"\bоткрой\b", f"{result}") and re.search(r"\bпапку\b", f"{result}"):
                folder = Linux_logic.defolt_folder_recognition(phrase=result)
                lin_log.open_defolt_folder(folder=folder)
            elif re.search(r"\bдобавить\b", f"{result}") and re.search(r"\bпапку\b", f"{result}"):
                lin_log.create_user_folder()
            elif re.search(r"\bвремя\b", f"{result}"):
                self.textEdit.setText(lin_log.time_system())
            elif re.search(r"\bхокку\b", f"{result}"):
                self.textEdit.setText(lin_log.hokky())
            elif re.search(r"\bпогода\b", f"{result}"):
                self.city_weather()

        if platform.system() == 'Windows':

            if re.search(r"\bбраузер\b", f"{result}") and re.search(r"\bоткрой\b", f"{result}"):
                self.browser(result, self.num_brow, 0)

            elif re.search(r"\bсайт\b", f"{result}") and re.search(r"\bоткрой\b", f"{result}"):
                result = wordskey.site_recog(result)
                self.browser(result, self.num_brow, 1)

            elif re.search(r"\bизменить\b", f"{result}") \
                    and re.search(r"\bбраузер\b", f"{result}") \
                    and platform.system() == 'Windows':
                self.num_brow = self.browser(result, self.num_brow, 2)
                if self.num_brow != 0 and self.num_brow != None:
                    webbrowser.register('Browser', None,
                                        webbrowser.BackgroundBrowser(self.num_brow))
                    self.num_brow = 'Browser'
                    self.textEdit.setText("- Браузер успешно изменен, пока я работаю).")

            elif re.search(r"\bоткрой\b", f"{result}") \
                    and ((re.search(r"\bпапку\b", f"{result}")) \
                         or (re.search(r"\bфайл\b", f"{result}"))):
                self.open_file_dir(result)

            elif re.search(r"\bпогода\b", f"{result}"):
                self.textEdit.setText(f"- Загружаю информацию о погоде...")
                city_and_weather(self.th_signal_weath, 1)

            elif re.search(r"\bвремя\b", f"{result}"):
                self.textEdit.setText(self.time_now("Время"))

            elif re.search(r"\bдата\b", f"{result}"):
                self.textEdit.setText(self.time_now("Дата"))

            elif re.search(r"\bсистема\b", f"{result}"):
                QMessageBox.about(self, "Ваша система", f"{self.system()}")

            elif re.search(r"\bорел\b", f"{result}") and re.search(r"\bрешка\b", f"{result}"):
                self.mon()

            elif re.search(r"\bпредсказание\b", f"{result}"):
                self.boll()

            elif re.search(r"\bумеешь\b", f"{result}") and re.search(r"\bчто\b", f"{result}"):
                self.start_manual.emit()

            elif re.search(r"\bхокку\b", f"{result}"):
                with open("Pickle/hokky.pickle", "rb") as h:
                    data = pickle.load(h)
                self.textEdit.setText(f"{data[random.randint(0, 7)]}")

            elif re.search(r"\bгде\b", f"{result}") and re.search(r"\bя\b", f"{result}"):
                self.textEdit.setText(f"- Загружаю информацию о вашем местположении...")
                city_and_weather(self.th_signal_loc, 0)

            elif re.search(r"\bновости\b", f"{result}"):
                self.browser(result, num_brow, 3)

            elif re.search(r"\bкороновирус\b", f"{result}"):
                self.browser(result, num_brow, 4)

            elif re.search(r"\b\d+[-+*/^]?\d+\b", f"{result}"):
                expr = re.search(r"\b\d+[-+*/^]?\d+\b", f"{result}")[0]
                self.textEdit.setText(f"{expr}={self.eval_math_expr(expr)}")

            elif re.search(r"\bтебя\b", f"{result}") and re.search(r"\bзовут\b", f"{result}") \
                    and re.search(r"\bкак\b", f"{result}"):
                self.textEdit.setText(f"- Мое имя {self.CW.config['User']['character']}")

            elif re.search(r"\bпока\b", f"{result}"):
                self.CW.flow.close()

            else:
                self.textEdit.setText("- Я вас не поняла.")

        self.CW.flow.start_new_gif.emit(self.CW.flow.speak_gif)
        self.CW.flow.timer_gif.emit(3000)


if __name__ == '__main__':
    # main()
    pass
