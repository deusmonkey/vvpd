# -*- coding: utf-8 -*-

"""Модулю для распознавания ключевых слов, если это сайты, вроде тут все понятно."""
import pickle

def site_load():
    site = {"http://vk.com": ["вк", "вконтакте", "vk"],
         "http://ok.ru": ["одноклассники"],
         "https://gitlab.com": ["гитлаб", "гит", "репозиторий"],
         "https://instagramm.com": ["инст", "интаграм", "инстаграмм", \
                                    "instagram", "instagramm"],
         "https://e.sfu-kras.ru": ["курсы","екурсы","сфу"],
         "https://youtube.com": ["ютюб", "youtube", "ютюбчик", "видео"],
         "https://gmail.com": ["мыло", "почта"],
         "http://yandex.ru": ["яндекс"],
         "https://google.com": ["google", "гугл"]}

    with open("Pickle/site_base.pickle", "wb") as f:
        pickle.dump(site, f)

    with open("Pickle/site_base.pickle", "rb") as f:
        x = pickle.load(f)

        print(x)


if __name__ == "__main__":
    site_load()


