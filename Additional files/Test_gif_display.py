import sys
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *


class MainWindow (QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow,self).__init__(parent)
        self.setGeometry(50,50,539,500)
        self.home()
        self.gif_display()
        
    def home(self):
        lab = QLabel("Example", self) # Creates the brew coffee button
        #but.clicked.connect(self.gif_display)
        self.show()
    

    def gif_display(self):
        path = 'C:/Gobot/VVPD_proj/vvpd/Gif_animations/Aqua_22'
        l = QMovieLabel(path, self)
        l.adjustSize()
        l.show()
        

class QMovieLabel(QLabel):
    def __init__(self, fileName, parent=None):
        super(QMovieLabel, self).__init__(parent)
        m = QMovie(fileName)
        self.setMovie(m)
        m.start()

    def setMovie(self, movie):
        super(QMovieLabel, self).setMovie(movie)
        s=movie.currentImage().size()
        self._movieWidth = s.width()
        self._movieHeight = s.height()

def run():
    app = QApplication(sys.argv)
    window = MainWindow()
    window.setAttribute(Qt.WA_NoSystemBackground, True)
    window.setWindowFlags(Qt.FramelessWindowHint)
    window.setAttribute(Qt.WA_TranslucentBackground, True)
    sys.exit(app.exec_())

if __name__ == '__main__':
    run()