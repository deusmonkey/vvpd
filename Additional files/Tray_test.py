import time
import sys
import types
import test_wind
from threading import Thread
import threading


from collections import OrderedDict
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
  
class ProgramTray(QThread):
    def __init__(self):
        
        QThread.__init__(self)
        self.menu = QMenu()
        self.icon = QSystemTrayIcon()
        self.flag_exit = True
  
    def run(self):
        """Код работающий в отдельном потоке"""
    
        while self.flag_exit:
            time.sleep(2)
            print ("I'm working ...")
  
        QApplication.quit()
  
    def stop(self):
        self.flag_exit = False
  
    def setMenu(self, menu=None):
        """Устанавливает пункты меню по клику на иконку в трее"""
  
        if not menu:
            menu = []
  
        # Создаем коллекцию не отсортированых элементов.
        collection = OrderedDict(menu)
        items = collection.keys()  # [имена пунктов]
        functions = collection.values()  # [функции. соотвествующие пунктам]
  
        for i, item in enumerate(items):
            function = functions[i]
  
            if isinstance(function, types.MethodType) \
                or isinstance(function, types.FunctionType):
                self.menu.addAction(QtGui.QAction(item, self,
                                    triggered=function))
  
        self.quitAction = QAction("Exit", self,
                                        triggered=self.stop)
        self.menu.addAction(self.quitAction)
  
        self.icon.setContextMenu(self.menu)
 
    
if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    #test_wind.main()
    program = ProgramTray()
    program.setMenu()
    program.icon.show()
    program.start()
  
    app.exec_()