import sys
from threading import Thread
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from collections import OrderedDict

def process():
    x = 4
    while x != 5:
        print("testing")

    
class MouseTracker(QMainWindow):
    def __init__(self, parent=None):
        super(MouseTracker, self).__init__(parent)
        self.setAttribute(Qt.WA_NoSystemBackground, True)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.initUI()
        self.setMouseTracking(True)

    def initUI(self):
        self.setGeometry(300, 300, 300, 200)
        self.setWindowTitle('Mouse Tracker')
        self.label = QLabel(self)
        self.label.resize(200, 40)
        self.show()
        
    def paintEvent(self, event=None):
        painter = QPainter(self)

        painter.setOpacity(0.5)
        painter.setBrush(Qt.white)
        painter.setPen(QPen(Qt.white))   
        painter.drawRect(self.rect())
        
    def mouseMoveEvent(self, event):
        self.test_event = event.x()
        print('Mouse coords: ( %d : %d )' % (event.x(), event.y()))
        

if __name__ == '__main__':
    #thread1 = Thread(target=process)
    #thread1.start()
    app = QApplication(sys.argv)
    ex = MouseTracker()
    sys.exit(app.exec_())
    #thread1.join()