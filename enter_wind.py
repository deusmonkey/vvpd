# -*- coding: utf-8 -*-

"""Поле ввода аквы"""
import sys
import pickle
import configparser
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QWidget, QCompleter, QApplication, QFrame, QStackedLayout, QGridLayout,\
    QLineEdit, QVBoxLayout, QHBoxLayout, QLabel
from PyQt5.QtCore import Qt, QMetaObject


class EnterForm(QWidget):
    """
    Класс ввода, который собирается из виджетов в один виджет.
    """

    def __init__(self):
        super().__init__()
        #self.scale = 110
        self.config = configparser.ConfigParser()
        self.config.read("Data/Config.ini")
        self.size_win = int(self.config["Settings"]["window_scale"])
        self.scale = int(int(self.config["Settings"]["window_scale"]) / 100 * 18)
        self.charact = self.config["User"]["character"]
        if self.charact == "Kaguya":
            self.bg_color = "rgba(255, 224, 224, 110)"
            self.border_color = "rgba(255, 193, 193, 255)"
            self.font_color = "rgba(147, 0, 0, 255)"
        if self.charact == "Aqua":
            self.bg_color = "rgba(119, 221, 254, 110)"
            self.border_color = "rgba(0, 168, 250, 255)"
            self.font_color = "rgba(0, 0, 224, 255)"
        self.setupUi()

    # Почти все здесь нужно для того, чтобы получить один виджет из нескольких виджетов,
    # каких я отметил
    def setupUi(self):
        self.frame = QFrame()
        self.frame.setFrameShape(QFrame.StyledPanel)
        self.frame.setLineWidth(0.6)
        mlay = QStackedLayout(self.frame)
        mlay.setStackingMode(1)

        self.bgFrame = QFrame(self.frame)  # Фрейм для фона
        self.UIFrame = QFrame(self.frame)
        add_lay = QGridLayout(self.bgFrame)

        # Фон
        self.pixmap = QPixmap("ImageGif/Image/Enter_field_bg2.png")
        width = 445 * self.size_win / 100
        height = 100 * self.size_win / 100
        self.mini_pix = self.pixmap.scaled(width, height, Qt.IgnoreAspectRatio, Qt.FastTransformation)
        self.back = QLineEdit(self.bgFrame)
        #self.back.setGeometry(QRect(0, 0, 445 * self.size_win / 100,
                                           #100 * self.size_win / 100))
        #self.back.setPixmap(self.mini_pix)
        self.back.setStyleSheet('''
                                background-color: %s;
                                border-width: 2px;
                                border-style: solid;
                                border-radius: 10px;
                                border-color: %s;
                                min-width: 10em;
                                min-height: 5em;
                                font: %s;
                                ''' %(self.bg_color, 
                                      self.border_color, 
                                      str(self.scale)+'px'))
        self.back.setObjectName("background")
        
        add_lay.addWidget(self.back)
        
        lay = QVBoxLayout(self.UIFrame)
        # lay.insertStretch(5)
        self.config = configparser.ConfigParser()
        self.config.read("Data/Config.ini")

        self.hframe = QFrame()
        hlay = QHBoxLayout(self.hframe)
        # Твое имя)))
        self.label = QLabel(self.hframe)
        self.label.setText(f"{self.config['User']['username']} ввели: ")
        self.label.setObjectName("UserName")
        self.label.setStyleSheet('''
                                    font:  %s;
                                    color: %s;
                                    font-style: italic;
                                    padding: 6px;
                                    ''' %(str(self.scale)+'px',
                                          self.font_color))
    
        self.say = QLabel(self.hframe)
        self.say.setText("Вводимый текст")
        self.say.setObjectName("UserSayed")
        self.say.setStyleSheet('''
                                    font:  %s;
                                    color: %s;
                                    font-style: italic;
                                    padding: 6px;
                                    ''' %(str(self.scale)+'px',
                                          self.font_color))
    
        hlay.addWidget(self.label)
        hlay.addWidget(self.say)
    
        # Поле ввода
        self.edit_line = QLineEdit(self.UIFrame)
        self.edit_line.setStyleSheet('''
                                     background-color: rgba(0, 0, 0, 00);
                                     border-width: 2px;
                                     border-style: solid;
                                     border-radius: 10px;
                                     border-color: %s;
                                     font:  %s;
                                     color: %s;
                                     font-style: italic;
                                     min-width: 10em;
                                     padding: 6px;
                                     ''' %(self.border_color,
                                           str(self.scale) + 'px',
                                           self.font_color))

        self.edit_line.setObjectName("TextLine")
        self.edit_line.setPlaceholderText("Введите команду...")

        key = ["открой браузер", "открой сайт", "изменить браузер", "открой папку", "открой файл",
               "погода", "время", "дата", "система", "орел и решка", "предсказание", "Что ты умеешь?", "хокку",
               "Где я?", "новости", "короновирус", "пока", "Как тебя зовут?"]
        self.completer = QCompleter(key, self.edit_line)
        self.completer.setCaseSensitivity(0)
        self.completer.setFilterMode(Qt.MatchContains)
        self.completer.popup().setStyleSheet('''
                                    background-color: white;
                                    border-width: 2px;
                                    border-style: solid;
                                    border-color: blue;
                                    font:  16px;
                                    color: blue;
                                    font-style: italic;
                                     ''')
        self.edit_line.setCompleter(self.completer)

        lay.addWidget(self.hframe)
        lay.addWidget(self.edit_line)

        mlay.addWidget(self.bgFrame)
        mlay.addWidget(self.UIFrame)
        self.setLayout(mlay)

        QMetaObject.connectSlotsByName(self.frame)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = EnterForm()
    w.show()
    app.exec_()
