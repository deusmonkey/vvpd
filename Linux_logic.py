# -*- coding: utf-8 -*-

"""
Так-ссс. Вообще не мое болото, поэтому могу сказать только, что здесь функцианал под Linux.
"""

import subprocess
import pickle
import getpass
import os
import easygui
import random
import datetime
import platform as pl

"My class"
class MainLogicLinux():
    """
    Класс для функционала под Linux.
    """

    with open("Pickle/site_base.pickle", "rb") as site:
        data_site = pickle.load(site)

    def open_browser(self):
        subprocess.call(["firefox"])

    def open_site(self, url):
        subprocess.call(["firefox", f"{url}"])

    def open_defolt_folder(self, folder):
        user_name = getpass.getuser()
        subprocess.call(["dolphin", f"/home/{user_name}/{folder}"])

    def time_system(self):
        return time_now(dat_time="Время")

    def hokky(self):
        with open("Pickle/hokky.pickle", "rb") as h:
            data = pickle.load(h)
        return data[random.randint(0, 7)]


def time_now(dat_time):
    now = datetime.datetime.now()
    answer = ["Время", "Дата"]
    if dat_time == answer[0]:
        return f"Сейчас - {now.hour}:{now.minute}:{now.second}. Пора смотреть аниме (^-^)!!!"
    elif dat_time == answer[1]:
        return f"Сегодняшняя дата - {now.date().day}-{now.date().month}-{now.date().year}. " \
               f"Не забудьте отдохнуть (^-^)."


def system():
    sys = pl.uname()
    return "Информация о системе:" \
           f"    Имя системы/OS - {sys[0]}\n" \
           f"    Сетевое имя компьютера - {sys[1]}\n" \
           f"    Выпуск системы - {sys[2]}\n" \
           f"    Версия выпуска системы - {sys[3]}\n" \
           f"    Тип машины - {sys[4]}\n" \
           f"    Имя процессора - {sys[5]}"

# Папки для открытия, почему здесь хз
def folder_base_creation():
    data_defolt_folder = {"Downloads": ["Downloads", "Загрузки", "загрузки"],
                          "Documents": ["Documents", "Документы", "документы", "доки"],
                          "Desktop" : ["Desktop", "Рабочий", "Стол", "стол", "рабочий"],
                          "Picture": ["Picture", "Изображения", "изображения"]}

    with open("Pickle/Linux_Defolt_Folder.pickle", "wb") as ldf:
        pickle.dump(data_defolt_folder, ldf)

# Обработка фраз, я так понимаю
def defolt_folder_recognition(phrase):
    print(phrase)
    with open("Pickle/Linux_Defolt_Folder.pickle", "rb") as word:
        site = pickle.load(word)
    answer = ""
    out_phrase = ""
    index = 0
    Flag = True
    while index != len(phrase):
        if phrase[index] == ".":
            return out_phrase
        elif phrase[index] != " ":
            answer += phrase[index]
            index += 1
        elif phrase[index] == " ":
            for key in site:
                for i in range(len(site[key])):
                    if answer == site[key][i]:
                        out_phrase += key + " "
                        answer = ""
                        return out_phrase
                    else:
                        Flag = False
        if Flag == False:
            answer = ""
            Flag = True
            index += 1
    print(out_phrase)
    return out_phrase

if __name__ == "__main__":
    print(defolt_folder_recognition(phrase="Загрузки "))
