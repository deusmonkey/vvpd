#!/usr/bin/env python3
# -*- coding: utf-8 -*-


"""Модуль, который содержит класса для регистрационного окна.
Опять же не мое болото, так что не хочу это коммитить.
Хотя вроде тут все понятно."""
import sys
import configparser

from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit,\
QStackedWidget, QPushButton, QHBoxLayout, QApplication, QFrame,\
QGridLayout, QStackedLayout, QMainWindow
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont, QPixmap, QIcon



import custom_buttons


class Widget1(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        lay = QStackedLayout(self)
        lay.setStackingMode(1)
        hello_lbl = QLabel()
        hello_lbl.setText ("""Добро пожаловать пользователь.\n 
Вас приветсвует окно регистрации в этой\n чудесной программе.
Осталось всего несколько шагов.""")
        hello_lbl.setStyleSheet('''font:  25px;
                                    color: #002756;
                                    font-style: oblique;
                                    ''')
        hello_lbl.setFont(QFont("Times"))
        hello_lbl.setAlignment(Qt.AlignCenter)

        lay.addWidget(hello_lbl)


class Widget2(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        lay = QVBoxLayout(self)
        lbl = QLabel()
        lbl.setText("Как помощнику к вам обращаться?")
        lbl.setStyleSheet('''font:  25px;
                              color: #002756;
                              font-style: oblique;
                              ''')
        lbl.setAlignment(Qt.AlignCenter)
        lbl.setFont(QFont("Times"))
        lay.addWidget(lbl)
        self.lbl2 = QLabel()
        self.lbl2.setText("")
        self.lbl2.setStyleSheet('''font:  25px;
                              color: #002756;
                              font-style: oblique;
                              ''')
        self.lbl2.setAlignment(Qt.AlignCenter)
        self.lbl2.setFont(QFont("Times"))
        lay.addWidget(self.lbl2)
        self.enter = QLineEdit()
        self.enter.setFont(QFont("Times", 16))
        lay.addWidget(self.enter)



class Widget3(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.character = ""
        lay = QVBoxLayout(self)
        self.buttonframe = QFrame(self)
        self.picframe = QFrame(self)
        self.card1 = QFrame(self.buttonframe)
        self.card2 = QFrame(self.buttonframe)
        buttonslay = QGridLayout(self.buttonframe)

        piclay1 = QStackedLayout(self.card1)
        piclay2 = QStackedLayout(self.card2)
        piclay1.setStackingMode(1)
        piclay2.setStackingMode(1)
        
        bgpixmap = QPixmap("ImageGif/Image/Button_bg.png")
        bgpixmap = bgpixmap.scaled(220, 220, Qt.IgnoreAspectRatio, Qt.FastTransformation)
        self.bg_label1 = QLabel()
        self.bg_label1.setPixmap(bgpixmap)
        self.bg_label2 = QLabel()
        self.bg_label2.setPixmap(bgpixmap)
        self.bg_label1.hide()
        self.bg_label2.hide()
        pixmap = QPixmap("ImageGif/Gif/Aqua_set/Icon.png")
        #pixmap = pixmap.scaled(215, 215, Qt.IgnoreAspectRatio, Qt.FastTransformation)
        pixmap_hover = pixmap
        pixmap_pressed = pixmap
        btn1 = custom_buttons.PicButton(pixmap, pixmap_hover, pixmap_pressed)
        btn1.clicked.connect(self.change_character1)
        pixmap = QPixmap("ImageGif/Gif/Kaguya_set/Icon.png")
        pixmap_hover = pixmap
        pixmap_pressed = pixmap
        btn2 = custom_buttons.PicButton(pixmap, pixmap_hover, pixmap_pressed)
        btn2.clicked.connect(self.change_character2)
        piclay1.addWidget(btn1)
        piclay2.addWidget(btn2)
        piclay1.addWidget(self.bg_label1)
        piclay2.addWidget(self.bg_label2)

        # print(piclay1.count())
        lbl = QLabel()
        lbl.setText ("""Здесь вы можете вбрать образ вашего помощника.
        Она или Он будут помогать вам в ваших обыденных делах за монитором.
        И вы всегда сможете сменить образ помощника если вам надоест.
        Не волнуйтесь. Они не обидчевы.""")
        lbl.setStyleSheet('''font:  20px;
                                    color: #002756;
                                    font-style: oblique;
                                    ''')
        lbl.setFont(QFont("Times"))
        lbl.setAlignment(Qt.AlignCenter)
        lay.addWidget(lbl)
        
        buttonslay.setRowStretch(0, 1)
        buttonslay.setRowStretch(1, 1)
        buttonslay.setRowStretch(2, 1)
        
        
        buttonslay.setColumnStretch(0, 1)
        buttonslay.setColumnStretch(1, 1)
        buttonslay.setColumnStretch(2, 1)
        buttonslay.setColumnStretch(3, 1)
        buttonslay.setColumnStretch(4, 1)
        buttonslay.addWidget(self.card1, 1, 1)
        buttonslay.addWidget(self.card2, 1, 3)
        lay.addWidget(self.buttonframe)
        self.err_lbl = QLabel("")
        self.err_lbl.setStyleSheet('''font:  20px;
                                            color: #002756;
                                            font-style: oblique;
                                            ''')
        lay.addWidget(self.err_lbl)
        #lay.addWidget(self.picframe)


    def change_character1(self):
        self.character = "Aqua"
        self.bg_label1.show()
        self.bg_label2.hide()

    def change_character2(self):
        self.character = "Kaguya"
        self.bg_label2.show()
        self.bg_label1.hide()


class Widget4(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        lay = QVBoxLayout(self)
        bye_lbl = QLabel()
        bye_lbl.setText ("""Поздравляю! Ваша регистрация завершена.
        Приятного время препровождения.""")
        bye_lbl.setStyleSheet('''font:  25px;
                                    color: #002756;
                                    font-style: oblique;
                                    ''')
        bye_lbl.setAlignment(Qt.AlignCenter)
        bye_lbl.setFont(QFont("Times"))
        lay.addWidget(bye_lbl)


class stackedExample(QWidget):
    def __init__(self, config, parent=None):
        QWidget.__init__(self, parent=parent)
        #self.setWindowFlags(Qt.FramelessWindowHint)
        #self.setStyleSheet("background-image: ImageGif/Image/Main_bg.jpg;")
        self.setWindowIcon(QIcon("ImageGif/Image/settings.png"))
        self.setFixedSize(700, 520)
        bg = QLabel()
        pixmap = QPixmap("ImageGif/Image/Main_bg.jpg")
        mini_pix = pixmap.scaled(850, 580, Qt.IgnoreAspectRatio, Qt.FastTransformation)
        bg.setPixmap(mini_pix)
        mlay = QStackedLayout(self)
        mlay.setStackingMode(1)
        self.config = config
        self.mframe = QFrame()
        lay = QVBoxLayout(self.mframe)
        self.W2 = Widget2()
        self.W3 = Widget3()
        self.Stack = QStackedWidget()
        self.Stack.addWidget(Widget1())
        self.Stack.addWidget(self.W2)
        self.Stack.addWidget(self.W3)
        self.Stack.addWidget(Widget4())
        self.btnNext = QPushButton("Next")
        self.btnNext.setStyleSheet('''  QPushButton:hover { background-color: white;
                                                    color: black;}
                        QPushButton:!hover {background-color: rgba(0, 0, 0, 00);
                                            border-width: 2px;
                                            border-style: solid;
                                            font:  25px;
                                            color: #002756;
                                            font-style: oblique;
                                            padding: 1px;}
                                             ''')
        self.btnNext.clicked.connect(self.onNext)
        self.btnNext.setFont(QFont("Times", 14))
        self.btnNext.setAutoDefault(True)
        #btnPrevious = QPushButton("Previous")
        #btnPrevious.clicked.connect(self.onPrevious)
        btnLayout = QHBoxLayout()
        #btnLayout.addWidget(btnPrevious)
        btnLayout.addWidget(self.btnNext)

        lay.addWidget(self.Stack)
        lay.addLayout(btnLayout)
        mlay.addWidget(self.mframe)
        mlay.addWidget(bg)

    def onNext(self):
        self.btnNext.show()
        if self.W2.enter.text() == "" and self.Stack.currentIndex() == 1:
            self.W2.lbl2.setText("Кажется вы забыли указать ваше имя")
            self.W2.enter.setFocus()
            return

        if self.W3.character == "" and self.Stack.currentIndex() == 2:
            self.W3.err_lbl.setText("Пожалуйста выберите персонажа")
            return

        if self.Stack.currentIndex() == 3:
            self.btnNext.setText("Finish")
            self.config["User"]["username"] = self.W2.enter.text()
            self.config["User"]["registered"] = "Yes"
            self.config["User"]["character"] = self.W3.character
            self.config["Exit"]["Answer_w1"] = "No"
            with open("Data/Config.ini", 'w') as configfile:
                self.config.write(configfile)
            self.close()

        if self.Stack.currentIndex() == 0:
            #self.btnNext.hide()
            self.W2.enter.setFocus()
            self.W2.enter.returnPressed.connect(self.onNext)


        self.Stack.setCurrentIndex(self.Stack.currentIndex()+1)


if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read("Data/Config.ini", encoding='cp1251')
    config["User"]["registered"] = "No"
    print("1")
    with open("Data/Config.ini", 'w', encoding='utf-8') as configfile:
                config.write(configfile)
    app = QApplication(sys.argv)
    w = stackedExample(config)
    w.show()
    app.exec_()