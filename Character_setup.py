# -*- coding: utf-8 -*-
"""
Created on Mon May 11 21:43:27 2020

@author: Freezee
"""

import sys
import configparser
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLineEdit,\
QStackedWidget, QPushButton, QHBoxLayout, QApplication, QFrame
from PyQt5 import QtCore
from PyQt5.QtGui import QFont, QPixmap



class Widget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent=parent)
        self.character = ""
        lay = QVBoxLayout(self)
        self.buttonframe = QFrame(self)
        self.picframe = QFrame(self)
        self.card1 = QFrame(self)
        self.card2 = QFrame(self)
        buttonlay = QHBoxLayout(self.buttonframe)
        
        piclay1 = QVBoxLayout(self.card1)
        piclay2 = QVBoxLayout(self.card2)

        btn1 = QPushButton("Аква")
        btn1.setFont(QFont("Times", 12))
        btn1.clicked.connect(self.change_character1)
        btn2 = QPushButton("Кагуя")
        btn2.setFont(QFont("Times", 12))
        btn2.clicked.connect(self.change_character2)
        piclay1.addWidget(btn1)
        piclay2.addWidget(btn2)
        piclay1.addSpacing(5)
        piclay2.addSpacing(5)
        lbl = QLabel()
        lbl.setText ("""Здесь вы можете вбрать образ вашего помощника.
        Она или Он будут помогать вам в ваших обыденных делах за монитором.
        И вы всегда сможете сменить образ помощника если вам надоест. 
        Не волнуйтесь. Они не обидчевы.""")
        lbl.setFont(QFont("Times", 14))
        lbl.setAlignment(QtCore.Qt.AlignCenter)
        lay.addWidget(lbl)
        
        self.pixmap = QPixmap("ImageGif/Gif/Aqua_set/Icon")
        self.mini_pix = self.pixmap.scaled(200, 200, QtCore.Qt.IgnoreAspectRatio, QtCore.Qt.FastTransformation)
        lbl1 = QLabel()
        lbl1.setAlignment(QtCore.Qt.AlignCenter)
        lbl1.setPixmap(self.mini_pix)
        piclay1.addWidget(lbl1)
        self.pixmap = QPixmap("ImageGif/Gif/Kaguya_set/Icon")
        self.mini_pix = self.pixmap.scaled(200, 200, QtCore.Qt.IgnoreAspectRatio, QtCore.Qt.FastTransformation)
        lbl2 = QLabel()
        lbl2.setAlignment(QtCore.Qt.AlignCenter)
        lbl2.setPixmap(self.mini_pix)
        piclay2.addWidget(lbl2)
        buttonlay.addWidget(self.card1)
        buttonlay.addWidget(self.card2)
        lay.addWidget(self.buttonframe)
        self.err_lbl = QLabel("")
        lay.addWidget(self.err_lbl)
        #lay.addWidget(self.picframe)
        
        
    def change_character1(self):
        self.character = "Aqua"
            
            
    def change_character2(self):
        self.character = "Kaguya"
        
        
class Character_setup(QWidget):
    def __init__(self, config, parent=None):
        QWidget.__init__(self, parent=parent)
        self.config = config
        self.W = Widget()
        lay = QVBoxLayout(self)
        self.bframe = QFrame(self)
        blay = QHBoxLayout(self.bframe)
        btn1 = QPushButton("Save")
        btn1.setFont(QFont("Times", 14))
        btn1.clicked.connect(self.onNext)
        btn2 = QPushButton("Cancel")
        btn2.setFont(QFont("Times", 14))
        btn2.clicked.connect(self.Cancel)
        blay.addWidget(btn1)
        blay.addWidget(btn2)
        self.Stack = QStackedWidget()
        self.Stack.addWidget(self.W)
        lay.addWidget(self.Stack)
        lay.addWidget(self.bframe)
        self.setLayout(lay)
        print("wut")
        
    
    def onNext(self):
        self.config["User"]["character"] = self.W.character
        with open("Data/Config.ini", 'w') as configfile:
            self.config.write(configfile)
        self.close()
        
    def Cancel(self):
        self.close()
        
"""
def main(config):
    app = QApplication(sys.argv)
    w = Character_setup(config)
    w.show()
    #app.exec_()
"""    

if __name__ == '__main__':

    config = configparser.ConfigParser()
    config.read("Data/Config.ini")
    with open("Data/Config.ini", 'w') as configfile:
                config.write(configfile)
    app = QApplication(sys.argv)
    w = Character_setup(config)
    w.show()
    app.exec_()

