# -*- coding: utf-8 -*-

"""Хз зачем этот тест-файл, поэтому я не собираюсь коммитеть его, я свои тест-файлы не кидаю)."""

import pickle
import speech_recognition as sr
import subprocess
import wordskey


def command_site():
    recor = sr.Recognizer()

    with sr.Microphone(device_index=None) as source:
        print("Говорите: ")
        recor.energy_threshold = 4000
        recor.dynamic_energy_threshold = True
        recor.pause_threshold = 1
        recor.adjust_for_ambient_noise(source, duration=1)
        audio = recor.listen(source)

    try:
        zadanie = recor.recognize_google(audio, language="ru-RU").lower()
        print("Вы сказали: " + zadanie)
        zadanie = wordskey.site_recog(zadanie + " ")
    except sr.UnknownValueError:
        print("Я вас не поняла.")
        zadanie = command_site()


    return zadanie


def open_browser():
    subprocess.call(["firefox"])


def open_site():
    with open("Pickle/site_base.pickle", "rb") as site:
        site_data = pickle.load(site)
    for key in site_data:
        print(key, "-", site_data[key])
    print("Какой сайт открыть для вас?")
    result = command_site()
    subprocess.call(["firefox", result])
