from __future__ import unicode_literals

import sys
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *



    
class PicButton(QAbstractButton):
    def __init__(self, pixmap, pixmap_hover, pixmap_pressed, parent=None):
        super(PicButton, self).__init__(parent)
        self.pixmap = pixmap
        self.pixmap_hover = pixmap_hover
        self.pixmap_pressed = pixmap_pressed

        self.pressed.connect(self.update)
        self.released.connect(self.update)
        
    def paintEvent(self, event):
        pix = self.pixmap_hover if self.underMouse() else self.pixmap
        if self.isDown():
            pix = self.pixmap_pressed

        painter = QPainter(self)
        painter.drawPixmap(event.rect(), pix)
        

    def enterEvent(self, event):
        self.update()
        

    def leaveEvent(self, event):
        self.update()
        

    def sizeHint(self):
        return QSize(200, 200)



if __name__ == "__main__":
    
    app = QApplication(sys.argv)
    window = QWidget()
    layout = QHBoxLayout(window)

    button = PicButton(QPixmap("C:/Gobot/VVPD_proj2/vvpd/ImageGif/Gif/Kaguya_set/Icon.png"),
                       QPixmap("C:/Gobot/VVPD_proj2/vvpd/ImageGif/Gif/Kaguya_set/Icon.png"),
                       QPixmap("C:/Gobot/VVPD_proj2/vvpd/ImageGif/Image/Button_bg.png"))
    layout.addWidget(button)
    button.setCheckable(True)
    


    window.show()
    sys.exit(app.exec_())


