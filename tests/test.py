# -*- coding: utf-8 -*-

import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)

sys.path.insert(0, parentdir)

#import Linux_logic
import wordskey


def test_1():
    assert wordskey.words_recog("открой") == "открой "

def test_2():
    assert wordskey.words_recog("сайт") == "сайт "

def test_3():
    assert wordskey.words_recog("открой ") == "открой "

def test_4():
    assert wordskey.words_recog("папку") == "папка "

def test_5():
    assert wordskey.words_recog("сайт") == "сайт "

def test_6():
    assert wordskey.words_recog("время") == "время "

def test_7():
    assert wordskey.words_recog("дата") == "дата "

def test_8():
    assert wordskey.words_recog("умеешь") == "умеешь "

def test_9():
    assert wordskey.words_recog("эй крошка что") == "что "

def test_10():
    assert wordskey.words_recog("короновирус") == "короновирус "

def test_11():
    assert wordskey.words_recog("сайт") == "сайт "

def test_12():
    assert wordskey.words_recog("расскажи мне новости") == "новости "

def test_13():
    assert wordskey.words_recog("пока девочка") == "пока "

def test_14():
    assert wordskey.words_recog("расскажи мне хокку") == "хокку "

def test_15():
    assert wordskey.words_recog("где я нахожусь ") == "где "

def test_16():
    assert wordskey.words_recog("файл") == "файл "

def test_17():
    assert wordskey.words_recog("вк") == "вконтакте "

def test_18():
    assert wordskey.words_recog("открой ") == "открой "

def test_19():
    assert wordskey.words_recog("аниме девочка") == "аниме "

def test_20():
    assert wordskey.words_recog("аниме") == "аниме "

def test_21():
    assert wordskey.words_recog("одноклассники ") == "одноклассники "

def test_22():
    assert wordskey.words_recog("яндекс") == "яндекс "

def test_23():
    assert wordskey.words_recog("открой сайт вк") == "открой сайт вконтакте "

#def test_24():
#    assert wordskey.words_recog("открой сайт инст") == "открой сайт инстагра "
#
#def test_25():
#    assert wordskey.words_recog("aткрой") == "открой "
#
#def test_26():
#    assert wordskey.words_recog("сайт") == "сайт "
#
#def test_27():
#    assert wordskey.words_recog("открой ") == "открой "
#
#def test_28():
#    assert wordskey.words_recog("aткрой") == "открой "
#
#def test_29():
#    assert wordskey.words_recog("сайт") == "сайт "
#
#def test_30():
#    assert wordskey.words_recog("открой ") == "открой "
#
#def test_31():
#    assert wordskey.words_recog("aткрой") == "открой "
#
#def test_32():
#    assert wordskey.words_recog("сайт") == "сайт "
#
#def test_33():
#    assert wordskey.words_recog("открой ") == "открой "
#
#def test_34():
#    assert wordskey.words_recog("aткрой") == "открой "
#
#def test_35():
#    assert wordskey.words_recog("сайт") == "сайт "
#
#def test_36():
#    assert wordskey.words_recog("открой ") == "открой "



if __name__ == "__main__":
    print("1")
    print(wordskey.words_recog("открой "))